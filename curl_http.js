const https = require('https');
const querystring = require('querystring');

function httpsget(url, retry_times) {
  return new Promise((resolve, reject) => {
    let data = '';

    const makeRequest = () => {
      https.get(url, (res) => {
        res.on('data', (chunk) => {
          data += chunk;
        });
        res.on('end', () => {
          resolve(data);
        });
      }).on('error', (err) => {
        if (retry_times > 0) {
          console.log(`Request ${url} failed. Retrying... (${retry_times} attempts left)`);
          retry_times--;
          setTimeout(makeRequest, 3000);
        } else {
          reject(err);
        }
      });
    };

    makeRequest();
  });
}

function url_encode(str) {
  if (str) {
    str = str.replace(/\n/g, '\r\n');
    str = querystring.escape(str);
  }
  return str;
}

function params(arguments) {
  return querystring.stringify(arguments);
}

module.exports = {
  httpsget,
  params,
  url_encode
};
