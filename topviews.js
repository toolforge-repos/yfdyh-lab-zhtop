const https = require('https');
const querystring = require('querystring');
const curl_http = require('./curl_http');
const mysql = require('mysql');
const isDev = process.env.DEV || false;

// TODO: 检查 connection。更多错误处理

const namespaces = {
  Wikipedia: true, Special: true, Category: true, Portal: true,
  Template: true, File: true
};

const top_url = 'https://wikimedia.org/api/rest_v1/metrics/pageviews/top/zh.wikipedia.org';
var isOk = 0;

async function fetchData(url) {
  try {
    const res = await curl_http.httpsget(url, 3);
    return JSON.parse(res);
  } catch (error) {
    console.error(error.stack);
  }
}

async function processTopArticles() {
  const top_date = new Date(Date.now() - 86400 * 1000).toISOString().slice(0, 10).replace(/-/g, '/');
  const all_access = [];
  const all_access_dict = {};
  
  // TODO: check for refresh
  try {
    const categorize = require('./categorize');
    const allAccessData = await fetchData(`${top_url}/all-access/${top_date}`);
    const desktopData = await fetchData(`${top_url}/desktop/${top_date}`);

    for (let v of allAccessData.items[0].articles) {
      let ns = v.article.match(/^(.*?):/);
      if (ns) ns = ns[1].replace('_talk', '');
      if (!namespaces[ns]) {
        all_access.push(v);
        all_access_dict[v.article] = all_access.length;
      }
    }

    for (let v of desktopData.items[0].articles) {
      let articleObj = all_access[all_access_dict[v.article] - 1];
      if (articleObj) {
        articleObj.positive = true;
        let div = v.views / articleObj.views;
        if (div > 0.95 || div < 0.05) {
          articleObj.positive = false;
        }
      }
    }

    var topart = all_access.filter(v => v.positive);
    // https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database#Steps_to_create_a_user_database_on_tools.db.svc.wikimedia.cloud
    // CREATE DATABASE s...__wmc
    // USE  DATABASE s...__wmc
    // CREATE TABLE zhtop (  ID INT AUTO_INCREMENT PRIMARY KEY, article VARCHAR(255),  type VARCHAR(255),  topic VARCHAR(255),  views INT);
    const connection = mysql.createConnection({
      host: isDev ? "127.0.0.1" : 'tools.db.svc.wikimedia.cloud',
      port: isDev ? 3307 : 3306,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: `${process.env.DB_USER}__wmc`,
      charset: 'utf8mb4'
    });
    console.log(`To be processed: ${topart.length} items.`)
    if (isDev) topart=topart.slice(0, 10); // for fast testing

    connection.query('TRUNCATE TABLE zhtop'); // TODO: keep history?
    const sqlValues = await Promise.all(topart.map(async item => {
      const cats = await categorize.getCats(item.article);
      const { artType, topic } = categorize.getTags(cats);
      return [item.article, artType, topic, item.views];
    }));

    const sqlStr = `INSERT INTO zhtop (article, type, topic, views) VALUES ?`;
    connection.query(sqlStr, [sqlValues], (error) => {
      if (error) {
        throw new Error(error);
      }
      connection.end();
      isOk = 1;
      console.log(`Successfully stored topviews for ${top_date}, length: ${sqlValues.length}`);
    });
  } catch (error) {
    isOk = -1;
    console.error(error.stack);
  }
}

//processTopArticles();

module.exports = {
  isOk,
  processTopArticles
}