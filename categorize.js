const mysql = require('mysql');
const isDev = process.env.DEV || false;

const cat_type = [
  ['演员#', 'star'], ['演員#', 'star'], ['歌手#', 'star'], ['音樂團體', 'star'],
  ['年出生', 'person'], ['年逝世', 'person'],
  ['城市#', 'place'],  ['国家#', 'place'],  ['國家#', 'place'], ['政權#', 'place'], ['共和国#', 'place'], ['成員國#', 'place'],
  ['节日#', 'festival'], ['節日#', 'festival'], ['主题日#', 'festival'],
  ['事件#', 'event'], ['案件#', 'event'], ['事故#', 'event'], ['罪案#', 'event'],
  ['网站#', 'website'], ['網站#', 'website']
];

const cat_topic = [
  ['電視劇', 'drama'], ['电视剧', 'drama'], ['漫畫', 'acg'], ['漫画', 'acg'],
  ['動畫', 'acg'], ['动画', 'acg'], ['遊戲#', 'acg'], ['游戏#', 'acg'], ['角色列表', 'acg'],
  ['劇集', 'drama'], ['剧集', 'drama'], ['電影#', 'film'], ['电影#', 'film'],
  ['色情', 'porn'], ['性行为', 'porn'], ['AV', 'porn'],
  ['黨', 'politics'], ['党', 'politics'], ['中共', 'politics'], ['政治', 'politics'], ['選舉', 'politics'], ['选举', 'politics'],
  ['军事', 'military'], ['軍事', 'military'], ['战争#', 'military'], ['戰爭#', 'military'], ['战役#', 'military'], ['戰役#', 'military'],
  ['真人秀', 'show'], ['綜藝', 'show'], ['综艺', 'show'], ['选秀', 'show'], ['娛樂節目', 'show'], ['娱乐节目', 'show'],
  ['演唱會', 'show'], ['演唱会', 'show'], ['音乐比赛', 'show'],
  ['冠狀病毒病', 'covid19'], ['冠状病毒病', 'covid19'],
  ['病', 'medicine'], ['药', 'medicine'], ['疫苗', 'medicine']
];

const exclusion = ['党员', '校友', '维基数据'];

function inExclusion(title) {
  for (let v of exclusion) {
    if (title.match(v)) {
      return true;
    }
  }
  return false;
}

const connection = mysql.createConnection({
  host: isDev ? "127.0.0.1" : 'zhwiki.analytics.db.svc.wikimedia.cloud',
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: 'zhwiki_p',
  charset: 'utf8mb4'
});

function getCats(article) {
  return new Promise((resolve, reject) => {
    const deCodeUtf = new TextDecoder();
    const sql = 'SELECT cl_to FROM categorylinks WHERE cl_from = (' +
      'SELECT page_id FROM page WHERE page_title = ? AND page_namespace = 0 AND page_is_redirect = 0)';
    connection.query({sql}, [article], (error, results) => {
      if (error) {
        reject(error);
      } else {
        resolve(results.map(result => deCodeUtf.decode(result.cl_to)));
      }
    });
  });
}

function getTags(categories) {
  categories = categories.map(x => inExclusion(x) ? '' : x).join('#') + '#';
  let artType = 'others';
  let topic = 'others';
  for (let v of cat_type) {
    if (categories.match(v[0])) {
      artType = v[1];
      break;
    }
  }
  for (let v of cat_topic) {
    if (categories.match(v[0])) {
      topic = v[1];
      break;
    }
  }
  return { artType, topic };
}

module.exports = {
  getCats,
  getTags
}