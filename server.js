const http = require('http');
const url = require('url');
const top = require('./topviews');
const schedule = require('node-schedule');
const port = process.env.PORT || 8000;
const isDev = process.env.DEV || false;

const mysql = require('mysql');
const fs = require('fs');
const { exit } = require('process');
var httpServer;
var dataLive = false;

initUser = function(){
	const filePath = (isDev ? "." : process.env.HOME) + '/replica.my.cnf';
	if (fs.existsSync(filePath)) {
		const conf = fs.readFileSync(filePath, 'utf8');
		const usernameMatch = conf.match(/\nuser = (\S+)/);
		const passwordMatch = conf.match(/\npassword = (\S+)/);
	  
		if (usernameMatch && passwordMatch) {
		  process.env.DB_USER = usernameMatch[1];
		  process.env.DB_PASSWORD = passwordMatch[1];
		} else {
		  console.warn('Username or password not found');
		}
	  } else {
		console.warn('replica.my.cnf file does not exist');
	  }
}()

toggleService = function(on) {
	dataLive = on;
}

initData = function() {
	top.processTopArticles().then(() => {
		toggleService(true);
	})
};

initWeb();
initData();

const job = schedule.scheduleJob('50 1 * * *', function(){
	toggleService(false);
	initData();
});

function waitForData(maxWaitTime, conditionFun) {
	let waitedTime = 0;
	const interval = 1000;
	return new Promise((resolve, reject) => {
	  const intervalId = setInterval(() => {
		waitedTime += interval;
		if (waitedTime >= maxWaitTime) {
		  clearInterval(intervalId);
		  reject("");
		}
		if (conditionFun()) {
		  clearInterval(intervalId);
		  resolve("");
		}
	  }, interval);
	});
}

function initWeb(){
	httpServer = http.createServer(function (req, res) {
		//res.end('Hello World\n');
		
		const parsedUrl = url.parse(req.url, true);
		const queryData = parsedUrl.query;
		if (parsedUrl.pathname == '/') {
			res.writeHead(200, { 'Content-Type': 'text/plain; charset=utf-8' });
			res.end(`It works.`);
		}
		else if (parsedUrl.pathname == '/test') {
			res.writeHead(200, { 'Content-Type': 'text/plain; charset=utf-8' });
			res.end(`发送的参数：${JSON.stringify(queryData)}`);
		}
		else if (parsedUrl.pathname == '/topviews') {
			/* if(!dataLive) {
				res.writeHead(503, { 'Content-Type': 'text/plain; charset=utf-8' });
				res.end(`Data is being prepared.`);
				return;
			} */
			let dataIsLive = function() {return dataLive};
			waitForData(60000, dataIsLive).then(()=> {
				gettop(queryData.type, queryData.topic, queryData.nrows || 10).then(a=>{
					res.writeHead(200, { "Access-Control-Allow-Origin": "*",
						'Content-Type': 'application/json; charset=utf-8' });
					res.end(a);
				});
			});
		}
		else {
			res.writeHead(404);
			res.end();
		}
	})
	httpServer.listen(port, () => {
		console.log(`started at http://localhost:${port}/`);
	});
}

// TODO: sql connection management
// TODO: 连接保持、数据库保持

const db = mysql.createConnection({
	host: isDev ? "127.0.0.1" : 'tools.db.svc.wikimedia.cloud',
	port: isDev ? 3307 : 3306,
	database: process.env.DB_USER + '__wmc',
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	charset: 'utf8mb4',
	multipleStatements: true // TODO
});
db.connect((err) => {
	if (err) {
		console.error('error connecting: ' + err.stack); // TOOD: retry
		exit()
	}
});

function generateCondition(column, str) {
	if (!str) return '';
	const cond = str.split('|');
	const included = [];
	const exempted = [];
	for (const v of cond) {
		const [flag, value] = v.match(/(!?)(.*)/).slice(1, 3);
		if (flag !== '') {
			exempted.push(mysql.escape(value));
		} else {
			included.push(mysql.escape(value));
		}
	}
	let sqlStr;
	if (exempted.length > 0) {
		sqlStr = `${column} NOT IN (${exempted.join(',')})`;
	} else if (included.length > 0) {
		sqlStr = `${column} IN (${included.join(',')})`;
	}
	return ' AND ' + sqlStr;
}

function gettop(type, topic, nrows = 10) {
	return new Promise((resolve, reject) => {
	  const sqlStr = 'SELECT article, type, topic, views FROM zhtop WHERE 1 = 1' +
		generateCondition('type', type) +
		generateCondition('topic', topic) +
		' ORDER BY id LIMIT ?';
  
	  db.query(sqlStr, [nrows], (error, results, fields) => {
		if (error) {
			reject(error);
		} else {
			const jsonData = JSON.stringify(results);
			resolve(jsonData);
		}
		//db.end();
	  });
	});
}
